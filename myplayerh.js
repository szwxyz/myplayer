
        function getUrlParams() {
            var params = {};
            var paramStr = window.location.search.substr(1);

            if (paramStr.length > 0) {
                var paramNameValuePairs = paramStr.split(';');

                for (var i = 0; i < paramNameValuePairs.length; i++) {
                    var param = paramNameValuePairs[i];

                    // 验证是否存在传递的参数
                    if (param.trim().length > 0) {
                        var indexOfEquals = param.indexOf('=');
                        if (indexOfEquals !== -1) {
                            var paramName = param.substr(0, indexOfEquals);
                            var paramValue = param.substr(indexOfEquals + 1);
                            params[paramName] = paramValue;
                        } else {
                            params[param] = '';
                        }
                    }
                }
            }

            return params;
        }
        var params = getUrlParams();

        //alert(params.url);
        //alert(params.s);
        var url = params.url;
        //alert(url);
        var videoType = url.match(/\.(mp4|avi|wmv|mov|flv|mkv|webm|m3u8|mpd)(\?.*)?$/i)[1];
