
            Artplayer.CONTEXTMENU = false; //关闭右键菜单
            Artplayer.RECONNECT_TIME_MAX = 3; //空链重连次数
            Artplayer.RECONNECT_SLEEP_TIME = 3000; //空链重连间隔
        var art = new Artplayer({
            id: 'szwxyz',
            container: '.artplayer-app',
            type: videoType,
            url: params.url, //http://vjs.zencdn.net/v/oceans.mp4
            lang: 'zh-cn', // or 'en'
            poster: '//www.5ytj.com/js/video.jpg', //预加载背景
            theme: '#ffad00', //主题色
            volume: 0.5, //默认音量
            muted: false,  //是否静音
            isLive: false, //直播模式
            autoplay: true, //自动播放
            autoSize: true, //自适应全屏
            autoMini: true, //迷你播放器
            loop: false, //循环播放
            flip: true, //视频翻转
            playbackRate: true, //显示倍速
            aspectRatio: true, //长宽比
            screenshot: true, //截图开关
            setting: true,//设置面板
            hotkey: true, //快捷键
            pip: true, //画中画
            mutex: false, //多开播一
            fullscreen: true, //窗口全屏
            fullscreenWeb: true, //网页全屏
            subtitleOffset: false, //字幕时间偏移范围在 [-5s, 5s]
            miniProgressBar: true, //迷你进度条
            playsInline: true,//移动端
            lock: true, //移动端屏锁
            fastForward: true, //移动端长按屏快进

            customType: {
                m3u8: playM3u8,
                flv: playFlv,
                mpd: playMpd,
            },
            
    layers: [
        {
            html: `<img style="width: 100px" src="https://www.5ytj.com/template/stmb22/statics/img/logo.png">`,
            style: {
                position: 'absolute',
                top: '0px',
                right: '0px',
            },
        },
    ],       //台标
    
    /*icons: {
        loading: '<img src="https://artplayer.org/assets/img/ploading.gif">', 
        state: '<img width="150" heigth="150" src="https://artplayer.org/assets/img/state.svg">',
        indicator: '<img width="16" heigth="16" src="https://artplayer.org/assets/img/indicator.svg">',
    },  //读取暂定图标          */
    <!--功能按钮结束-->
});



        art.on('ready', () => {
            art.seek = params.s;
        });
        function playM3u8(video, url, art) {
            if (Hls.isSupported()) {
                if (art.hls) art.hls.destroy();
                const hls = new Hls();
                hls.loadSource(url);
                hls.attachMedia(video);
                art.hls = hls;
                art.on('destroy', () => hls.destroy());
            } else if (video.canPlayType('application/vnd.apple.mpegurl')) {
                video.src = url;
            } else {
                art.notice.show = 'Unsupported playback format: m3u8';
            }
        }
        function playFlv(video, url, art) {
            if (flvjs.isSupported()) {
                if (art.flv) art.flv.destroy();
                const flv = flvjs.createPlayer({ type: 'flv', url });
                flv.attachMediaElement(video);
                flv.load();
                art.flv = flv;
                art.on('destroy', () => flv.destroy());
            } else {
                art.notice.show = 'Unsupported playback format: flv';
            }
        }
        function playMpd(video, url, art) {
            if (dashjs.supportsMediaSource()) {
                if (art.dash) art.dash.destroy();
                const dash = dashjs.MediaPlayer().create();
                dash.initialize(video, url, art.option.autoplay);
                art.dash = dash;
                art.on('destroy', () => dash.destroy());
            } else {
                art.notice.show = 'Unsupported playback format: mpd';
            }
        }
